FROM openjdk:11.0.10-jdk
ARG JAR_FILE=target/*.jar
COPY target/helloworld-0.0.1-SNAPSHOT.jar helloworld-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/helloworld-0.0.1-SNAPSHOT.jar"]